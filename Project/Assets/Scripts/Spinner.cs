﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour
{

	public float SpinSpeed;
	public bool SpinChildren;
	public float ChildSpinSpeed;
	public bool d;
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate (0, 0, SpinSpeed * Time.deltaTime);
		if (d)
			Debug.Log (GetComponentsInChildren<Transform> ().Length);
		if (SpinChildren)
			foreach (Transform t in GetComponentsInChildren<Transform>()) {
				if (t == transform)
					continue;
				t.Rotate (0, 0, ChildSpinSpeed * Time.deltaTime);
			}
	}
}
