﻿using UnityEngine;
using System.Collections;

public class Floater : MonoBehaviour
{

	public float Speed = 1;
	public float Spin = 1;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (Speed * Time.deltaTime, 0, 0);
		transform.Rotate (0, 0, Spin * Time.deltaTime);
	}
}
