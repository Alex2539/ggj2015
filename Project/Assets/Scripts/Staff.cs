﻿using UnityEngine;
using System.Collections;

public class Staff : MonoBehaviour
{
	public float SpawnX;
	public float LaneWidth;
	public GameObject gemPrefab;
	public GameObject notePrefab;
	public GameObject endPrefab;
	public float DestroyX;
	public float speed;
	public float spawnOffset;
	public Song song;
	public bool chordModifier = false;
	public bool rest = false;

	private float tileDistance;
	private float maxSpeed;

	// Use this for initialization
	void Start ()
	{
		maxSpeed = speed;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (-speed * Time.deltaTime, 0, 0);
		tileDistance += speed * Time.deltaTime;
		if (speed == 0)
			spawnOffset += maxSpeed * Time.deltaTime;
		foreach (Transform t in GetComponentsInChildren<Transform>()) {
			if (t == transform)
				continue;
			if (t.position.x < DestroyX)
				Destroy (t.gameObject);
		}
	}

	//Instantiates the object at position
	public void spawnObject (float position, float i, bool isGem, float duration, bool stemmed)
	{
		GameObject spawn;
		if (isGem) {
			spawn = (GameObject)Instantiate (gemPrefab);
			Gem g = spawn.GetComponent<Gem> ();
			g.SetColour ((Gem.Colour)Random.Range (0, 3));
		} else {
			spawn = (GameObject)Instantiate (notePrefab);
			spawn.GetComponent<Note> ().Duration = duration;
			if (stemmed)
				spawn.GetComponent<Note> ().AddStem (i > 1);
		}
		
		spawn.transform.parent = transform;
		spawn.transform.position = new Vector3 (SpawnX + position + spawnOffset, transform.position.y - 1.5f + i * LaneWidth, 0f);
	}

	public void spawnEnding (float position)
	{
		GameObject spawn = (GameObject)Instantiate (endPrefab);
		spawn.transform.parent = transform;
		spawn.transform.position = new Vector3 (SpawnX + position, transform.position.y, 0f);
		foreach (Transform t in GetComponentsInChildren<Transform>()) {
			if (t == transform)
				continue;
			if (t.position.x > spawn.transform.position.x)
				Destroy (t.gameObject);
		}
	}
}
