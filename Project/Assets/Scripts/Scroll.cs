﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour {

	public GameObject notePrefab;
	public float Speed;
	private float time = 1;
	private float time2 = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(-Speed*Time.deltaTime, 0, 0);
		time += Time.deltaTime;
		time2 += Time.deltaTime;
		//print (transform.localPosition.x);
		if (transform.localPosition.x < -20) {
			transform.localPosition = (new Vector3(2, 0, 10));
		}
	}
}
