﻿using UnityEngine;
using System.Collections;

public class VictoryMessage : MonoBehaviour
{
	public Player Player1, Player2;
	public PlayerInput Controller;
	private bool disabled = true;
	// Use this for initialization
	void Start ()
	{
		renderer.enabled = false;
		disabled = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (disabled)
			return;
		Player1.Pause ();
		Player2.Pause ();
		if (Controller.Green ())
			Application.LoadLevel (0);
	}

	public void Show ()
	{
		disabled = false;
		renderer.enabled = true;
	}
}
