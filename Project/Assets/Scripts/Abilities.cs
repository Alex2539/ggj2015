﻿using UnityEngine;
using System.Collections;

public class Abilities
{
	private static readonly float YYBPAUSETIME = 8f;
	private static readonly float YYRPAUSETIME = 4f;
//	private static readonly int LANEDIFFERENCE = 6;

	public static void RRY (Player p)
	{
		p.Controls.reversed = true;
	}

	public static void RRB (Player p)
	{
		p.aFog.renderer.enabled = true;
	}

	public static void YYR (Player p)
	{
		p.setPauseTime (YYRPAUSETIME);
		p.Pause ();
	}

	public static void YYB (Player p)
	{
		p.setPauseTime (YYBPAUSETIME);
	}

	public static void BBR (Player p)
	{
		int randomLane = Random.Range (0, 4);
		p.illegalLane = randomLane;
	}

	public static void BBY (Player p)
	{
		p.renderer.enabled = false;
	}

	public static void RRR (Player p)
	{
		p.aStaff.chordModifier = true;
	}

	public static void YYY (Player p)
	{
	}

	public static void BBB (Player p)
	{
		float LANEDIFFERENCE = Mathf.Abs (p.aStaff.transform.position.y - p.otherPlayer.aStaff.transform.position.y);
		if (p.StartY > 0) {
			p.StartY -= LANEDIFFERENCE;
			p.transform.position = new Vector3 (p.transform.position.x, p.transform.position.y - LANEDIFFERENCE, p.transform.position.z);
			p.otherPlayer.StartY += LANEDIFFERENCE;
			p.otherPlayer.transform.position = new Vector3 (p.otherPlayer.transform.position.x, p.otherPlayer.transform.position.y + LANEDIFFERENCE, p.otherPlayer.transform.position.z);
		} else {
			p.StartY += LANEDIFFERENCE;
			p.transform.position = new Vector3 (p.transform.position.x, p.transform.position.y + LANEDIFFERENCE, p.transform.position.z);
			p.otherPlayer.StartY -= LANEDIFFERENCE;
			p.otherPlayer.transform.position = new Vector3 (p.otherPlayer.transform.position.x, p.otherPlayer.transform.position.y - LANEDIFFERENCE, p.otherPlayer.transform.position.z);
		}
		Staff s = p.aStaff;
		p.aStaff = p.otherPlayer.aStaff;
		p.otherPlayer.aStaff = s;

		Inventory i = p.Gems;
		p.Gems = p.otherPlayer.Gems;
		p.otherPlayer.Gems = i;

		int n = p.Gems.Controller.PlayerNumber;
		p.Gems.Controller.PlayerNumber = p.otherPlayer.Gems.Controller.PlayerNumber;
		p.otherPlayer.Gems.Controller.PlayerNumber = n;

	}

	public static void RYB (Player p)
	{
		p.aStaff.rest = true;
	}
}
