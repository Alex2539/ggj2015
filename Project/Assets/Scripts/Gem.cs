﻿using UnityEngine;
using System.Collections;

public class Gem : MonoBehaviour
{

	public enum Colour
	{
		BLUE,
		YELLOW,
		RED
	}

	public Colour GemColour;
	public AudioClip[] HitSounds;
	public Sprite BlueSprite, YellowSprite, RedSprite;
	public bool Collected { get { return collected; } }
	public float CollectSpeed = 15;
	private bool collected;
	private bool attach = false;
	private SpriteRenderer sr;

	private Transform target;
	private float moveSpeed;

	void Awake ()
	{
		sr = GetComponent<SpriteRenderer> ();
	}

	void Update ()
	{
		if (target != null) {
			Vector3 direction = target.position - transform.position;
			float distance = direction.magnitude;
			direction.Normalize ();
			if (moveSpeed * Time.deltaTime > distance) {
				transform.Translate (distance * direction, Space.World);
				target = null;
			} else
				transform.Translate (moveSpeed * Time.deltaTime * direction, Space.World);
		}
	}

	public void SetColour (Colour c)
	{
		GemColour = c;
		if (sr == null)
			return;
		switch (c) {
		case Colour.BLUE:
			sr.sprite = BlueSprite;
			break;
		case Colour.YELLOW:
			sr.sprite = YellowSprite;
			break;
		case Colour.RED:
			sr.sprite = RedSprite;
			break;
		default:
			break;
		}
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		GameObject go = c.gameObject;
		Player p = go.GetComponent<Player> ();
		if (p != null) {
			if (p.Gems != null) {
				MoveToTarget (p.Gems.GetStorageSlot (GemColour), CollectSpeed, true);
				p.Gems.AddGem (GemColour);
				gameObject.layer = 5;
				if (audio != null && HitSounds.Length > 0) {
					audio.clip = HitSounds [Random.Range (0, HitSounds.Length - 1)];
					audio.Play ();
				}
			}
			collected = true;
//			Destroy (gameObject);
		}
	}

	public void MoveToTarget (Transform target, float speed, bool attach)
	{
		this.target = target;
		moveSpeed = speed;
		if (attach)
			transform.parent = target;
		this.attach = attach;
	}
}
