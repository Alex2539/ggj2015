﻿using UnityEngine;
using System.Collections;

public class DifficultySettings : MonoBehaviour
{
	public static float[,] NoteProbs = new float[,] {
	// 1/8	1/4 	1/2 	1/1
		{0.25f,	0.50f,	0.15f,	0.10f},	// Easy
		{0.40f,	0.40f,	0.15f,	0.07f},	// Medium
		{0.66f,	0.17f,	0.10f,	0.05f}	// Hard
	};

	public static float[,] ChordProbs = new float[,] {
	//One	Two 	Three
		{0.75f,	0.20f,	0.05f},	//Eighth
		{0.66f,	0.17f,	0.17f},	//Quarter
		{0.05f,	0.55f,	0.40f},	//Half
		{0.00f,	0.00f,	1.00f}	//Whole
	};

	public static float[] Notes = new float[] { 0.125f, 0.25f, 0.5f, 1f};
	public static float[] GemProbs = new float[] {0, 0.25f, 0.33f};

	public enum Levels
	{
		EASY,
		MEDIUM,
		HARD
	}
	public Levels Difficulty;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	public float GetNoteLength ()
	{
		float[] probs = new float[NoteProbs.GetLength (1)];
		for (int i=0; i!=NoteProbs.GetLength(1); ++i) {
			probs [i] = NoteProbs [(int)Difficulty, i];
		}

		int p = 0;
		float r = Random.value;
		while (r > 0 && p < probs.Length) {
			float prob = probs [p];
			if (r > prob) {
				r -= prob;
				++p;
			} else
				break;
		}

		return Notes [p];
	}

	public int GetChord (float note)
	{
		int n = 0;
		for (n=0; n!=Notes.Length; ++n) {
			if (note <= Notes [n]) {
				break;
			}
		}
		if (n == Notes.Length)
			--n;

		float[] probs = new float[ChordProbs.GetLength (1)];
		for (int i=0; i!=ChordProbs.GetLength(1); ++i) {
			probs [i] = ChordProbs [n, i];
		}

		int p = 0;
		float r = Random.value;
		while (r > 0 && p < probs.Length) {
			float prob = probs [p];
			if (r > prob) {
				r -= prob;
				++p;
			} else
				break;
		}

		return p + 1;
	}
}
