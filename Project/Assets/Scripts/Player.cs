﻿using UnityEngine;
using System.Collections.Generic;

public class Player : MonoBehaviour
{

	public int Lanes = 4;
	public float LaneWidth;
	public PlayerInput Controls;
	public VictoryMessage Victory;
	public float StartX;
	public float StartY;
	public int StartLane = 0; 
	public Inventory Gems;
	public NoteGenerator notes;
	public Player otherPlayer;
	public Staff aStaff;
	public Transform aFog;

	public AudioClip MoveSound;
	public AudioClip PauseOpponentSound;
	public AudioClip ShieldSound;
	public AudioClip FlipStavesSound;
	public AudioClip InvisibleSound;
	public AudioClip BlockLaneSound;
	public AudioClip FogSound;
	public AudioClip[] HitSounds;
	public enum Ability
	{
		NONE,
		BBB,
		YYY,
		RRR,
		RRB,
		RRY,
		YYB,
		YYR,
		BBY,
		BBR,
		RYB
	}
	
	private int[] gemBuffer = new int[3];
	private float maxSpeed;
	private int currLane = 1;

	public Dictionary<Ability, float> abilityMap = new Dictionary<Ability, float> ();
	public int illegalLane = -1;

	public readonly float ORIGINALPAUSEBEATS = 4;
	private readonly float YYB = 10; //Time it takes for Additional Pause Time to stop
	private readonly float RRR = 20; //Time it takes for Additional Chords to stop
	private readonly float BBY = 10; //Time it takes for Invisibility to stop
	private readonly float RRY = 10; //Time it takes for Reversed Keys to stop
	private readonly float YYY = 10; //TIme it takes for Invincible Shield to stop
	private readonly float BBR = 10; //Time it takes for Destroy a Lane to stop
	private readonly float RRB = 10; //Time it takes for fog to stop
	private readonly float RYB = 10; //Time it takes for rest to stop

	//Needed for pausing
	public float pauseBeats = 4;
	private float pausetime = 0;
	private float prevtime = 0;
	private float curtime = 0;

	private Animator anim;

	// Use this for initialization
	void Start ()
	{
		aFog.renderer.enabled = false;
		maxSpeed = aStaff.speed;
		transform.localPosition = new Vector3 (StartX, StartY, 10f);
		setPauseTime (pauseBeats);
		anim = GetComponent<Animator> ();
		currLane = StartLane;
	}
	
	// Update is called once per frame
	void Update ()
	{
		curtime += Time.deltaTime;
		int lane = currLane;
		if (Controls.Up ()) {
			if (abilityMap.ContainsKey (Ability.BBR)) {
				if (currLane + 1 == illegalLane && illegalLane != 3) {
					currLane += 2;
				} else if (currLane + 1 != illegalLane || illegalLane != 3) {
					currLane++;
				}
			} else {
				currLane++;
			}
		} else if (Controls.Down ()) {
			if (abilityMap.ContainsKey (Ability.BBR)) {
				if (currLane - 1 == illegalLane && illegalLane != 0) {
					currLane -= 2;
				} else if (currLane - 1 != illegalLane || illegalLane != 0) {
					currLane--;
				}
			} else {
				currLane--;
			}
		}
		if (currLane < 0)
			currLane = 0;
		if (currLane >= Lanes)
			currLane = Lanes - 1;

		if (currLane != lane && audio != null && MoveSound != null) {
			audio.clip = MoveSound;
			audio.Play ();
		}

		transform.position = new Vector3 (StartX, StartY + currLane * LaneWidth - 1.5f, 0);

		if (Controls.Green ()) {
			executeAbility ();
		}

		//Normal pause check
		if (aStaff.speed == 0) {
			if (curtime - prevtime > pausetime) { //Checks for pause 
				aStaff.speed = maxSpeed;
				if (abilityMap.ContainsKey (Ability.YYY)) {
					setPauseTime (ORIGINALPAUSEBEATS);
				}
				Controls.disabled = false;
			}
		}
		List<Ability> abilityList = new List<Ability> (abilityMap.Keys);
		foreach (Ability key in abilityList) {
			print (key);
			switch (key) {
			//Additional Pause Time check
			case Ability.YYB:
				if (curtime - abilityMap [Ability.YYB] > YYB) {
					setPauseTime (ORIGINALPAUSEBEATS);
					abilityMap.Remove (Ability.YYB);
				}
				break;

			//Instant Pause check
			case Ability.YYR: 
				if (curtime - abilityMap [Ability.YYR] > pausetime) {
					abilityMap.Remove (Ability.YYR);
				}
				break;

			//Additional Chords check
			case Ability.RRR: 
				if (curtime - abilityMap [Ability.RRR] > RRR) {
					aStaff.chordModifier = false;
					abilityMap.Remove (Ability.RRR);
				}
				break;

			//Invisibility check
			case Ability.BBY:
				if (curtime - abilityMap [Ability.BBY] > BBY) {
					renderer.enabled = true;
					abilityMap.Remove (Ability.BBY);
				}
				break;
				
			//Reversed Keys check
			case Ability.RRY:
				if (curtime - abilityMap [Ability.RRY] > RRY) {
					Controls.reversed = false;
					abilityMap.Remove (Ability.RRY);
				}
				break;
				
			//Invincibility check
			case Ability.YYY:
				if (curtime - abilityMap [Ability.YYY] > YYY) {
					abilityMap.Remove (Ability.YYY);
				}
				break;

			//Destroy a Lane check
			case Ability.BBR:
				if (curtime - abilityMap [Ability.BBR] > BBR) {
					abilityMap.Remove (Ability.BBR);
					illegalLane = -1;
				}
				break;
			
			//Fog check
			case Ability.RRB:
				if (curtime - abilityMap [Ability.RRB] > RRB) {
					abilityMap.Remove (Ability.RRB);
					aFog.renderer.enabled = false;
				}
				break;
			case Ability.RYB:
				if (curtime - abilityMap [Ability.RYB] > RYB) {
					abilityMap.Remove (Ability.RYB);
					aStaff.rest = false;

				}
				break;
			}
		}
	}

	//Pauses the game by pausing the notegenerator
	public void Pause (bool playSound = false)
	{
		if (abilityMap.ContainsKey (Ability.YYY)) {
			return;
		}
		prevtime = curtime;
		Controls.disabled = true;
		aStaff.speed = 0;

		if (playSound && audio != null && HitSounds.Length > 0) {
			audio.clip = HitSounds [Random.Range (0, HitSounds.Length - 1)];
			audio.Stop ();
			audio.Play ();
		}

		if (anim != null)
			anim.SetTrigger ("Hit");
	}

	//Sets prevtime to start the ability countdown
	public void startAbility ()
	{
		prevtime = curtime;
	}

	//Sets the amount of time to pause for whenever this player pauses
	public void setPauseTime (float beats)
	{
		if (aStaff.song != null)
			pausetime = beats / (aStaff.song.BPM / 60f);
	}

	//Executes the ability depending on what is in the buffer
	void executeAbility ()
	{
		foreach (Gem.Colour? c in Gems.Slots) {
			if (c.Equals (Gem.Colour.BLUE)) {
				gemBuffer [0]++;
			} else if (c.Equals (Gem.Colour.YELLOW)) {
				gemBuffer [1]++;
			} else if (c.Equals (Gem.Colour.RED)) {
				gemBuffer [2]++;
			}
		}

		int size = gemBuffer [0] + gemBuffer [1] + gemBuffer [2];
		if (size != 3) {
			clearGemBuffer ();
		}

		if (gemBuffer [0] == 3 && gemBuffer [1] == 0 && gemBuffer [2] == 0) {
			Debug.Log ("Swap Places");
			Abilities.BBB (otherPlayer);
			if (audio != null && FlipStavesSound != null) {
				audio.clip = FlipStavesSound;
				audio.Play ();
			}
		} else if (gemBuffer [0] == 0 && gemBuffer [1] == 3 && gemBuffer [2] == 0) {
			Debug.Log ("Invincibility");
			if (audio != null && ShieldSound != null) {
				audio.clip = ShieldSound;
				audio.Play ();
			}
			if (abilityMap.ContainsKey (Ability.YYY)) {
				abilityMap.Remove (Ability.YYY);
			}
			abilityMap.Add (Ability.YYY, curtime);
			Abilities.YYY (this);
		} else if (gemBuffer [0] == 0 && gemBuffer [1] == 0 && gemBuffer [2] == 3) {
			Debug.Log ("Additional Chords");
			if (otherPlayer.abilityMap.ContainsKey (Ability.RRR)) {
				otherPlayer.abilityMap.Remove (Ability.RRR);
			}
			otherPlayer.abilityMap.Add (Ability.RRR, curtime);
			Abilities.RRR (otherPlayer);
		} else if (gemBuffer [0] == 2 && gemBuffer [1] == 1 && gemBuffer [2] == 0) {
			if (audio != null && InvisibleSound != null) {
				audio.clip = InvisibleSound;
				audio.Play ();
			}
			Debug.Log ("Invisibility");
			if (otherPlayer.abilityMap.ContainsKey (Ability.BBY)) {
				otherPlayer.abilityMap.Remove (Ability.BBY);
			}
			otherPlayer.abilityMap.Add (Ability.BBY, curtime);
			Abilities.BBY (otherPlayer);
		} else if (gemBuffer [0] == 0 && gemBuffer [1] == 1 && gemBuffer [2] == 2) {
			Debug.Log ("Reversed Keys");
			if (otherPlayer.abilityMap.ContainsKey (Ability.RRY)) {
				otherPlayer.abilityMap.Remove (Ability.RRY);
			}
			otherPlayer.abilityMap.Add (Ability.RRY, curtime);
			Abilities.RRY (otherPlayer);
		} else if (gemBuffer [0] == 0 && gemBuffer [1] == 2 && gemBuffer [2] == 1) {
			if (audio != null && PauseOpponentSound != null) {
				audio.clip = PauseOpponentSound;
				audio.Play ();
			}
			Debug.Log ("Instant Pause");
			if (otherPlayer.abilityMap.ContainsKey (Ability.YYR)) {
				otherPlayer.abilityMap.Remove (Ability.YYR);
			}
			otherPlayer.abilityMap.Add (Ability.YYR, curtime);
			Abilities.YYR (otherPlayer);
		} else if (gemBuffer [0] == 1 && gemBuffer [1] == 2 && gemBuffer [2] == 0) {
			Debug.Log ("Increased Pause Time");
			if (otherPlayer.abilityMap.ContainsKey (Ability.YYB)) {
				otherPlayer.abilityMap.Remove (Ability.YYB);
			}
			otherPlayer.abilityMap.Add (Ability.YYB, curtime);
			Abilities.YYB (otherPlayer);
		} else if (gemBuffer [0] == 2 && gemBuffer [1] == 0 && gemBuffer [2] == 1) {
			if (audio != null && BlockLaneSound != null) {
				audio.clip = BlockLaneSound;
				audio.Play ();
			}
			Debug.Log ("Destroy a Lane");
			if (otherPlayer.abilityMap.ContainsKey (Ability.BBR)) {
				otherPlayer.abilityMap.Remove (Ability.BBR);
			}
			otherPlayer.abilityMap.Add (Ability.BBR, curtime);
			Abilities.BBR (otherPlayer);
		} else if (gemBuffer [0] == 1 && gemBuffer [1] == 0 && gemBuffer [2] == 2) {
			Debug.Log ("FOOOOOGGGG");
			if (audio != null && FogSound != null) {
				audio.clip = FogSound;
				audio.Play ();
			}
			if (otherPlayer.abilityMap.ContainsKey (Ability.RRB)) {
				otherPlayer.abilityMap.Remove (Ability.RRB);
			}
			otherPlayer.abilityMap.Add (Ability.RRB, curtime);
			Abilities.RRB (otherPlayer);
		} else if (gemBuffer [0] == 1 && gemBuffer [1] == 1 && gemBuffer [2] == 1) {
			Debug.Log ("RESSSSTT");
			if (abilityMap.ContainsKey (Ability.RYB)) {
				abilityMap.Remove (Ability.RYB);
			}
			abilityMap.Add (Ability.RYB, curtime);
			Abilities.RYB (this);
		}

		if (Gems.Slots [0] != null &&
		    Gems.Slots [1] != null &&
		    Gems.Slots [2] != null){
				clearGemBuffer ();
				Gems.Slots = new Gem.Colour?[3];
				Gems.slotIndex = 0;
				return;
		}
	}

	//Adds leftover gems in the buffer back into the inventory
	void addGemBuffer ()
	{
		for (int i = 0; i < gemBuffer[0]; i++) {
			Gems.AddGem (Gem.Colour.BLUE);
		}
		for (int i = 0; i < gemBuffer[1]; i++) {
			Gems.AddGem (Gem.Colour.YELLOW);
		}
		for (int i = 0; i < gemBuffer[2]; i++) {
			Gems.AddGem (Gem.Colour.RED);
		}
		clearGemBuffer ();
	}

	//Clears the buffer
	void clearGemBuffer ()
	{
		gemBuffer [0] = 0;
		gemBuffer [1] = 0;
		gemBuffer [2] = 0;
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		if (c.gameObject.tag.Equals ("Finish")) {
			Victory.Show ();
		}
	}
}
