﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour
{
	public int Blue, Yellow, Red;
	public int MaxGems = 5;
	public PlayerInput Controller;
	public AudioClip UseGem, UseAbility;
	public Transform[] BlueStorage = new Transform[3];
	public Transform[] YellowStorage = new Transform[3];
	public Transform[] RedStorage = new Transform[3];
	public Transform[] UsedGems = new Transform[3];
	public Transform Trash;

	public Gem.Colour?[] Slots = new Gem.Colour?[3];
	public int slotIndex = 0;
	private int blueIndex, yellowIndex, redIndex;

	// Use this for initialization
	void Start ()
	{
		float xpos = Camera.main.aspect * Camera.main.orthographicSize - 3;
		transform.position = transform.position + new Vector3 (xpos, 0, 0);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Controller.Green ()) {
			if (Slots [0] != null &&
				Slots [1] != null &&
				Slots [2] != null) {
				Gem g = UsedGems [0].GetComponentInChildren<Gem> ();
				g.MoveToTarget (Trash, g.CollectSpeed, true);
				g = UsedGems [1].GetComponentInChildren<Gem> ();
				g.MoveToTarget (Trash, g.CollectSpeed, true);
				g = UsedGems [2].GetComponentInChildren<Gem> ();
				g.MoveToTarget (Trash, g.CollectSpeed, true);
				if (audio != null && UseAbility != null) {
					audio.clip = UseAbility;
					audio.Play ();
				}
			}
		}
		Gem.Colour? colour = null;
		if (Controller.Blue () && Blue > 0) {
			colour = Gem.Colour.BLUE;
			--Blue;
		} else if (Controller.Yellow () && Yellow > 0) {
			colour = Gem.Colour.YELLOW;
			--Yellow;
		} else if (Controller.Red () && Red > 0) {
			colour = Gem.Colour.RED;
			--Red;
		}

		if (colour != null) {
			if (audio != null && UseGem != null) {
				audio.clip = UseGem;
				audio.Play ();
			}
			int targetInd;
			if (slotIndex < 3) {
				Slots [slotIndex] = colour;
				targetInd = slotIndex;
				++slotIndex;
			} else {
				Slots [0] = Slots [1];
				Slots [1] = Slots [2];
				Slots [2] = colour;
				targetInd = 2;
				Gem g1 = UsedGems [0].GetComponentInChildren<Gem> ();
				Gem g2 = UsedGems [1].GetComponentInChildren<Gem> ();
				Gem g3 = UsedGems [2].GetComponentInChildren<Gem> ();
				g1.MoveToTarget (Trash, g1.CollectSpeed, true);
				g2.MoveToTarget (UsedGems [0], g2.CollectSpeed, true);
				g3.MoveToTarget (UsedGems [1], g3.CollectSpeed, true);
			}
			if (colour == Gem.Colour.BLUE) {
				int ind = (blueIndex + MaxGems - 1) % MaxGems;
				Gem g = BlueStorage [ind].GetComponentInChildren<Gem> ();
				g.MoveToTarget (UsedGems [targetInd], g.CollectSpeed, true);
				blueIndex = ind;
			}
			if (colour == Gem.Colour.YELLOW) {
				int ind = (yellowIndex + MaxGems - 1) % MaxGems;
				Gem g = YellowStorage [ind].GetComponentInChildren<Gem> ();
				g.MoveToTarget (UsedGems [targetInd], g.CollectSpeed, true);
				yellowIndex = ind;
			}
			if (colour == Gem.Colour.RED) {
				int ind = (redIndex + MaxGems - 1) % MaxGems;
				Gem g = RedStorage [ind].GetComponentInChildren<Gem> ();
				g.MoveToTarget (UsedGems [targetInd], g.CollectSpeed, true);
				redIndex = ind;
			}
		}
	}

	public void AddGem (Gem.Colour Colour)
	{
		switch (Colour) {
		case Gem.Colour.BLUE:
			Blue++;
			if (Blue > MaxGems)
				Blue = MaxGems;
			break;
		case Gem.Colour.YELLOW:
			Yellow++;
			if (Yellow > MaxGems)
				Yellow = MaxGems;
			break;
		case Gem.Colour.RED:
			Red++;
			if (Red > MaxGems)
				Red = MaxGems;
			break;
		}
	}

	public void RemoveGem (Gem.Colour Colour)
	{
		switch (Colour) {
		case Gem.Colour.BLUE:
			Blue++;
			break;
		case Gem.Colour.YELLOW:
			Yellow++;
			break;
		case Gem.Colour.RED:
			Red++;
			break;
		}
	}

	public void RemoveGem (Gem.Colour[] Colours)
	{
		foreach (Gem.Colour c in Colours) {
			RemoveGem (c);
		}
	}

	public Transform GetStorageSlot (Gem.Colour colour)
	{
		if (colour == Gem.Colour.BLUE) {
			if (Blue < MaxGems) {
				Transform t = BlueStorage [blueIndex];
				blueIndex = (blueIndex + 1) % MaxGems;
				return t;
			} else {
				return Trash;
			}
		}

		if (colour == Gem.Colour.YELLOW) {
			if (Yellow < MaxGems) {
				Transform t = YellowStorage [yellowIndex];
				yellowIndex = (yellowIndex + 1) % MaxGems;
				return t;
			} else {
				return Trash;
			}
		}

		if (colour == Gem.Colour.RED) {
			if (Red < MaxGems) {
				Transform t = RedStorage [redIndex];
				redIndex = (redIndex + 1) % MaxGems;
				return t;
			} else {
				return Trash;
			}
		}

		return Trash;
	}
}
