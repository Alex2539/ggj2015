﻿using UnityEngine;
using System.Collections;

public class Note : MonoBehaviour
{
	private float duration;
	public float Duration {
		get { return duration; }
		set {
			duration = value;
			UpdateSprite ();
		}
	}
	public Sprite[] Heads = new Sprite[2];
	public GameObject[] Stems = new GameObject[3];
	private SpriteRenderer sr;

	void Awake ()
	{
		sr = GetComponent<SpriteRenderer> ();
	}
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void AddStem (bool down)
	{
//		bool down = transform.localPosition.y > 0;
//		Debug.Log (transform.localPosition.y + " " + down);
		if (duration > 0.125f) {
			GameObject stem = (GameObject)Instantiate (Stems [0]);
			stem.transform.parent = transform;
			if (down)
				stem.transform.localPosition = new Vector3 (-0.5f, -2.25f, 0);
			else
				stem.transform.localPosition = new Vector3 (0.5f, 2.25f, 0);
		} else {
			if (down) {
				GameObject stem = (GameObject)Instantiate (Stems [2]);
				stem.transform.parent = transform;
				stem.transform.localPosition = new Vector3 (0f, -2.25f, 0);
			} else {
				GameObject stem = (GameObject)Instantiate (Stems [1]);
				stem.transform.parent = transform;
				stem.transform.localPosition = new Vector3 (1f, 2.25f, 0);
			}
		}
	}

	private void UpdateSprite ()
	{
		if (sr == null)
			return;
		if (duration <= 0.25f) {
			sr.sprite = Heads [0];
		} else {
			sr.sprite = Heads [1];
		}
//		AddStem ();
	}

	void OnTriggerEnter2D (Collider2D c)
	{
		GameObject go = c.gameObject;
		Player p = go.GetComponent<Player> ();
		if (p != null) {
			p.Pause (true);
		}
		Destroy (gameObject);
	}
}
