﻿using UnityEngine;
using System.Collections;

public class FloatingShapes : MonoBehaviour
{

	public GameObject Floater;
	public Sprite[] Shapes;

	public float FloaterInterval;
	public float MinSpeed, MaxSpeed;
	public float MinRot, MaxRot;
	public float Radius;

	private float timer = 0;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		foreach (Transform t in GetComponentsInChildren<Transform>()) {
			if (t == transform)
				continue;
			Vector3 dist = t.position - transform.position;
			if (t.localPosition.sqrMagnitude >= Radius * Radius + 1) {
				Debug.Log (t.localPosition.sqrMagnitude + " " + (Radius * Radius + 1));
				Destroy (t.gameObject);
			}
		}

		timer += Time.deltaTime;
		if (timer >= FloaterInterval) {
			timer -= FloaterInterval;
			GameObject go = (GameObject)Instantiate (Floater);
			SpriteRenderer sr = go.GetComponent<SpriteRenderer> ();
			if (sr != null && Shapes.Length > 0)
				sr.sprite = Shapes [Random.Range (0, Shapes.Length - 1)];
			go.transform.Rotate (0, 0, Random.value * 360);
			float angle = Random.value * Mathf.PI * 2;
			go.transform.position = new Vector3 (Radius * Mathf.Cos (angle), Radius * Mathf.Sin (angle), 0);
			go.transform.parent = transform;
			Floater f = go.GetComponent<Floater> ();
			if (f != null) {
				f.Speed = Random.Range (MinSpeed, MaxSpeed);
				f.Spin = Random.Range (MinRot, MaxRot);
			}
		}
	}
}
