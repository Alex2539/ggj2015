﻿using UnityEngine;
using System.Collections;

public class NoteGenerator : MonoBehaviour
{
	
	public GameObject notePrefab;
	public GameObject gemPrefab;
	public GameObject BarLinePrefab;
	public Song CurrentSong {
		get { return song;}
		set {
			song = value;
			if (song == null)
				setBPM (0);
			else {
				setBPM (song.BPM);
				Difficulty.Difficulty = song.Difficulty;
				startTime = song.StartDelay + StartDelay;
			}
			Player1Staff.song = song;
			Player2Staff.song = song;
		}
	}
	public float BPM;
	public float Speed;
	public float BarLength;
	public int BeatsPerBar;
	public float BarSpawnX;
	public Staff Player1Staff;
	public Staff Player2Staff;
	public DifficultySettings Difficulty;
	public Song DebugSong;
	public int chordModifier = 0;

	private Song song;
	private float time = 0;
	private float time2 = 0;
	private float bps;
	private float barTime;
	private float barSpeed;
	private float barQuarterLength; 
	private bool done = false;
	private float startTimer = 0;
	private float startTime = 0;
	public float StartDelay = 1;
	
	// Use this for initialization
	void Start ()
	{
		if (DebugSong != null) {
			CurrentSong = DebugSong;
			setBPM (song.BPM);
			Difficulty.Difficulty = song.Difficulty;
			startTime = song.StartDelay + StartDelay;
		}
	}
	
	private void setBPM (float bpm)
	{
		BPM = bpm;
		bps = BPM / 60f;
		barTime = BeatsPerBar / bps;
		barQuarterLength = BarLength / 4f;
		barSpeed = BarLength / barTime;
		Player1Staff.speed = barSpeed;
		Player2Staff.speed = barSpeed;
		Speed = barSpeed;
	}
	
	// Update is called once per frame
	void Update ()
	{
		//		transform.Translate (-Speed * Time.deltaTime, 0, 0);
		time += Time.deltaTime;
		time2 += Time.deltaTime;
		startTimer += Time.deltaTime;

		if (!done && startTimer >= StartDelay && time > 4 / bps) {
			createBar ();
			time = 0;
		}

		if (startTimer >= startTime)
			song.Play ();
	}
	
	void createBar ()
	{
		float xPos = BarSpawnX;
		int bar = BeatsPerBar * 2;
		GameObject barLine = (GameObject)Instantiate (BarLinePrefab);
		barLine.transform.parent = transform;
		barLine.transform.localPosition = new Vector3 (xPos + time2 * barSpeed, 0, 0);
		while (bar > 0) {
			float noteLen = 0;
			if (bar == 1)
				noteLen = 1f / BeatsPerBar / 2f;
			else
				do {
					noteLen = Difficulty.GetNoteLength ();
				} while ((int)(noteLen*8) > bar);
			float noteSpace = barQuarterLength / 2f * noteLen * 8;
			createNote (xPos + noteSpace / 2f, noteLen);
			xPos += barQuarterLength / 2f * noteLen * 8;
			bar -= (int)(noteLen * 8);
		}
		
		if (time2 >= song.Length) {
			float playerDistance = Mathf.Abs (Player2Staff.transform.position.x - Player1Staff.transform.position.x);
			Player1Staff.spawnEnding (xPos + barQuarterLength / 4f + Player1Staff.transform.position.x < Player2Staff.transform.position.x ? 0 : playerDistance);
			Player2Staff.spawnEnding (xPos + barQuarterLength / 4f + Player1Staff.transform.position.x > Player2Staff.transform.position.x ? 0 : playerDistance);
			done = true;
		}
	}
	//Creates 1, 2, or 3 notes/gems in a column with threshold cutoffs affecting  probabilities
	void createNote (float position, float duration)
	{
		ArrayList staves = new ArrayList ();
		staves.Add (Player1Staff);
		staves.Add (Player2Staff);
		int chord = Difficulty.GetChord (duration);
		if (chord == 1) {
			spawn1 (position, duration, staves);
		} else if (chord == 2) {
			spawn2 (position, duration, staves);
		} else {
			spawn3 (position, duration, staves);
		}
	}
	
	//Spawns 3 notes/gems in a column
	void spawn3 (float position, float duration, ArrayList p)
	{
		bool stemmed = false;
		int spawnRand = Random.Range (0, 3);
		for (int i = 0; i < 4; i++) {
			if (i == spawnRand) {
				i += 1;
			}
			bool isGem = Random.value < DifficultySettings.GemProbs [2];
			foreach(Staff playerStaff in p){
				if(!playerStaff.rest){
					playerStaff.spawnObject (position, i, isGem, duration, !stemmed);
					stemmed = true;
				}
			}
		}
	}
	
	//Spawns 2 notes/gems in a column
	void spawn2 (float position, float duration, ArrayList p)
	{
		bool stemmed = false;
		int spawnRand = Random.Range (0, 3);
		int spawnRand2 = Random.Range (0, 3);
		while (spawnRand == spawnRand2) {
			spawnRand2 = Random.Range (0, 3);
		}
		int spawnRand3 = Random.Range (0, 3);
		while (spawnRand3 == spawnRand2 || spawnRand3 == spawnRand) {
			spawnRand3 = Random.Range (0, 3);
		}
		for (int i = 0; i < 4; i++) {
			bool isGem = Random.value < DifficultySettings.GemProbs [1];
			foreach(Staff playerStaff in p){
				if(!playerStaff.rest){
					if(!playerStaff.chordModifier){
						if (i == spawnRand || i == spawnRand2) {
							playerStaff.spawnObject (position, i, isGem, duration, !stemmed);
							stemmed = true;
						}
					} else{
						if (i == spawnRand || i == spawnRand2 || i == spawnRand3) {
							playerStaff.spawnObject (position, i, isGem, duration, !stemmed);
							stemmed = true;
						}
					}
				}
			}
		}
	}
	
	//Spawns 1 note in a column
	void spawn1 (float position, float duration, ArrayList p)
	{
		bool stemmed = false;
		int spawnRand = Random.Range (0, 3);
		int spawnRand2 = Random.Range (0, 3);
		while (spawnRand == spawnRand2) {
			spawnRand2 = Random.Range (0, 3);
		}
		bool isGem = Random.value < DifficultySettings.GemProbs [0];
		for (int i = 0; i < 4; i++) {
			foreach(Staff playerStaff in p){
				if(!playerStaff.rest){
					if(!playerStaff.chordModifier){
						if (i == spawnRand) {
							playerStaff.spawnObject (position, i, isGem, duration, true);
						}
					} else{
						if (i == spawnRand || i == spawnRand2) { 
							playerStaff.spawnObject (position, i, isGem, duration, !stemmed);
							stemmed = true;
						}
					}
				}
			}
		}
	}
}
