﻿using UnityEngine;
using System.Collections;

public class Song : MonoBehaviour
{

	public AudioClip Clip;
	public float BPM;
	public DifficultySettings.Levels Difficulty;
	public float Length;
	public float StartDelay;

	public void Play ()
	{	
		if (audio == null || audio.isPlaying)
			return;
		audio.clip = Clip;
		audio.Play ();
	}

	public void Pause ()
	{
		if (audio == null)
			return;
		audio.Pause ();
	}

	public void Stop ()
	{
		if (audio == null)
			return;
		audio.Stop ();
	}

	// Use this for initialization
	void Awake ()
	{
		Length = Clip.length;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
