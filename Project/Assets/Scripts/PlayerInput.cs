﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour
{
	public static float JoystickDeadzone = 0.25f;
	public int PlayerNumber = 1;
	public bool disabled = false;
	public bool reversed = false;

	private bool upPressed = false;
	private bool upPressedLast = false;

	private bool downPressed = false;
	private bool downPressedLast = false;

	private bool leftPressed = false;
	private bool leftPressedLast = false;

	private bool rightPressed = false;
	private bool rightPressedLast = false;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!reversed) {
			if (PlayerNumber < 1 || PlayerNumber > 4)
				return;
			
			if ((Input.GetKey (KeyCode.W) && PlayerNumber == 1) || (Input.GetKey (KeyCode.UpArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_YAxis_" + PlayerNumber) > JoystickDeadzone || 
			    Input.GetAxis ("DPad_YAxis_" + PlayerNumber) > JoystickDeadzone) {
				if (!upPressedLast)
					upPressed = true;
				else
					upPressed = false;
				upPressedLast = true;
			} else {
				upPressedLast = false;
				upPressedLast = false;
			}
			
			if ((Input.GetKey (KeyCode.S) && PlayerNumber == 1) || (Input.GetKey (KeyCode.DownArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_YAxis_" + PlayerNumber) < -JoystickDeadzone || 
			    Input.GetAxis ("DPad_YAxis_" + PlayerNumber) < -JoystickDeadzone) {
				if (!downPressedLast)
					downPressed = true;
				else
					downPressed = false;
				downPressedLast = true;
			} else {
				downPressedLast = false;
				downPressedLast = false;
			}
			
			if ((Input.GetKey (KeyCode.A) && PlayerNumber == 1) || (Input.GetKey (KeyCode.LeftArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_XAxis_" + PlayerNumber) < -JoystickDeadzone || 
			    Input.GetAxis ("DPad_XAxis_" + PlayerNumber) < -JoystickDeadzone) {
				if (!leftPressedLast)
					leftPressed = true;
				else
					leftPressed = false;
				leftPressedLast = true;
			} else {
				leftPressedLast = false;
				leftPressedLast = false;
			}
			
			if ((Input.GetKey (KeyCode.D) && PlayerNumber == 1) || (Input.GetKey (KeyCode.RightArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_XAxis_" + PlayerNumber) > -JoystickDeadzone || 
			    Input.GetAxis ("DPad_XAxis_" + PlayerNumber) > -JoystickDeadzone) {
				if (!rightPressedLast)
					rightPressed = true;
				else
					rightPressed = false;
				rightPressedLast = true;
			} else {
				rightPressedLast = false;
				rightPressedLast = false;
			}
		} else {
			if (PlayerNumber < 1 || PlayerNumber > 4)
				return;
			
			if ((Input.GetKey (KeyCode.W) && PlayerNumber == 1) || (Input.GetKey (KeyCode.UpArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_YAxis_" + PlayerNumber) > JoystickDeadzone || 
			    Input.GetAxis ("DPad_YAxis_" + PlayerNumber) > JoystickDeadzone) {
				if (!downPressedLast)
					downPressed = true;
				else
					downPressed = false;
				downPressedLast = true;
			} else {
				downPressedLast = false;
				downPressedLast = false;
			}
			
			if ((Input.GetKey (KeyCode.S) && PlayerNumber == 1) || (Input.GetKey (KeyCode.DownArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_YAxis_" + PlayerNumber) < -JoystickDeadzone || 
			    Input.GetAxis ("DPad_YAxis_" + PlayerNumber) < -JoystickDeadzone) {
				if (!upPressedLast)
					upPressed = true;
				else
					upPressed = false;
				upPressedLast = true;
			} else {
				upPressedLast = false;
				upPressedLast = false;
			}
			
			if ((Input.GetKey (KeyCode.A) && PlayerNumber == 1) || (Input.GetKey (KeyCode.LeftArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_XAxis_" + PlayerNumber) < -JoystickDeadzone || 
			    Input.GetAxis ("DPad_XAxis_" + PlayerNumber) < -JoystickDeadzone) {
				if (!rightPressedLast)
					rightPressed = true;
				else
					rightPressed = false;
				rightPressedLast = true;
			} else {
				rightPressedLast = false;
				rightPressedLast = false;
			}
			
			if ((Input.GetKey (KeyCode.D) && PlayerNumber == 1) || (Input.GetKey (KeyCode.RightArrow) && PlayerNumber == 2) ||
			    Input.GetAxis ("L_XAxis_" + PlayerNumber) > -JoystickDeadzone || 
			    Input.GetAxis ("DPad_XAxis_" + PlayerNumber) > -JoystickDeadzone) {
				if (!leftPressedLast)
					leftPressed = true;
				else
					leftPressed = false;
				leftPressedLast = true;
			} else {
				leftPressedLast = false;
				leftPressedLast = false;
			}
		}
	}

	public bool Up ()
	{
		if (disabled) {
			return false;
		}
		return upPressed;
	}

	public bool Down ()
	{
		if (disabled) {
			return false;
		}
		return downPressed;
	}

	public bool Left ()
	{
		if (disabled) {
			return false;
		}
		return leftPressed;
	}

	public bool Right ()
	{
		if (disabled) {
			return false;
		}
		return rightPressed;
	}

	public bool Green ()
	{
		return (Input.GetKeyDown (KeyCode.Space) && PlayerNumber == 1) || (Input.GetKeyDown (KeyCode.Return) && PlayerNumber == 2) || Input.GetButtonDown ("A_" + PlayerNumber);
	}

	public bool Blue ()
	{
		return (Input.GetKeyDown (KeyCode.Alpha1) && PlayerNumber == 1) || (Input.GetKeyDown (KeyCode.Alpha8) && PlayerNumber == 2) || Input.GetButtonDown ("X_" + PlayerNumber);
	}

	public bool Yellow ()
	{
		return (Input.GetKeyDown (KeyCode.Alpha2) && PlayerNumber == 1) || (Input.GetKeyDown (KeyCode.Alpha9) && PlayerNumber == 2) || Input.GetButtonDown ("Y_" + PlayerNumber);
	}

	public bool Red ()
	{
		return (Input.GetKeyDown (KeyCode.Alpha3) && PlayerNumber == 1) || (Input.GetKeyDown (KeyCode.Alpha0) && PlayerNumber == 2) || Input.GetButtonDown ("B_" + PlayerNumber);
	}
}
