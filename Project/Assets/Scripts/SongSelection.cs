﻿using UnityEngine;
using System.Collections;

public class SongSelection : MonoBehaviour
{

	public PlayerInput Controller;
	public GameObject SelectedSong;
	public GameObject[] Songs;
	// Use this for initialization

	void Awake ()
	{
		DontDestroyOnLoad (gameObject);
	}
	void OnLevelWasLoaded ()
	{
		if (SelectedSong != null) {
			NoteGenerator ng = FindObjectOfType<NoteGenerator> ();
			if (ng != null) {
				Debug.Log ("Loaded");
				GameObject go = (GameObject)Instantiate (SelectedSong);
				ng.CurrentSong = go.GetComponent<Song> ();
				Destroy (gameObject);
			}
		}

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Controller.Blue ()) {
			SelectedSong = Songs [0];
			Application.LoadLevel (1);
		}
		if (Controller.Yellow ()) {
			SelectedSong = Songs [1];
			Application.LoadLevel (1);
		}
		if (Controller.Red ()) {
			SelectedSong = Songs [2];
			Application.LoadLevel (1);
		}
	}
}
